FROM python:3-slim-buster
SHELL ["/bin/bash", "-c"]
WORKDIR /app
# Install Joplin
RUN apt-get update \
  && apt-get install --no-install-recommends -y curl git build-essential \
  && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
  && apt-get install --no-install-recommends -y libsecret-1-dev nodejs \
  && NPM_CONFIG_PREFIX=/joplin-bin npm install --unsafe-perm -g joplin \
  && ln -s /joplin-bin/bin/joplin /usr/bin/joplin \
  && apt-get purge -y git build-essential \
  && rm -rf /var/lib/apt/lists/*

# Install project dependancies
RUN mkdir /app/joplin-web
COPY manage.py requirements.txt MANIFEST.in setup.cfg setup.py    /app/joplin-web/
RUN export JW_BASE=/app \
  && cd "${JW_BASE}" \
  && python3 -m venv joplin-web \
  && cd joplin-web \
  && source bin/activate \
  && pip install -r requirements.txt

# Install supervisor launcher
RUN  pip install supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Install joplin-web code source
COPY joplin_web /app/joplin-web/joplin_web

VOLUME /data
EXPOSE 8001
CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

